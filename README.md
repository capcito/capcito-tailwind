# @capcito/tailwind
This package contains the base TailwindCSS configuration and stylesheet.

## Installation
Install the package via `npm install @capcito/tailwind` or `yarn add @capcito/tailwind`. The package contains a `tailwind.config.js` and `default.css` which you can include in your project in the way you prefer.

Please note that the PurgeCSS options provided by tailwind is not included, so you'll have to define them yourself..

For example:
```
const defaultConfig = require('@capcito/tailwind/tailwind.config')

module.exports = {
  ...defaultConfig,
  purge: {
    content: [
      './components/**/*.vue',
    ]
  }
}
```
