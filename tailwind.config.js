const plugin = require('tailwindcss/plugin')

module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
    standardFontWeights: true,
    defaultLineHeights: true,
  },
  theme: {
    fontFamily: {
      serif: ['"Financier Display"', 'Times New Roman'],
      sans: ['"Avenir"', 'Verdana'],
    },
    fontWeight: {
      normal: 300,
      semibold: 600,
      bold: 800,
    },
    colors: {
      blue: {
        100: '#E6EFFA',
        300: '#4DB2FF',
        500: '#308FFB',
        700: '#006DC1',
      },
      gray: {
        100: '#FAFAFA',
        200: '#DDDDDD',
      },
      red: {
        500: '#FB5530',
      },
      yellow: {
        500: '#FFA800',
        100: '#FFF6E5',
      },
      indigo: {
        100: '#D6DBE0',
        200: '#BFC7CF',
        300: '#80909F',
        400: '#40596F',
        500: '#01213F',
      },
      ivory: {
        300: '#FBF5EE',
        500: '#F6ECE1',
      },
      black: '#000000',
      white: '#FFFFFF',
      transparent: 'transparent',
    },
    boxShadow: {
      none: 'none',
      default: '0 3px 12px rgba(0, 0, 0, 0.08)',
      md: '0px 5px 15px rgba(0, 0, 0, 0.05)',
      lg: '0px 25px 45px rgba(0, 0, 0, 0.05)',
      xl: '0px 25px 45px rgba(0, 0, 0, 0.1)',
      white: '0 3px 12px rgba(255, 255, 255, 1)',
      'white-top': '0 -3px 12px rgba(255, 255, 255, 1)',
    },
    extend: {
      borderWidth: {
        3: '3px',
        6: '6px',
      },
      borderOpacity: {
        16: '0.16',
      },
      opacity: {
        2:  '0.02',
        5:  '0.05',
        10: '0.1',
        30: '0.3',
        60: '0.6',
      },
      padding: {
        '3.5': '0.875rem',
        '4.5': '1.125rem',
        22: '5.5rem',
        26: '6.5rem',
        31: '7.5rem',
        41: '10.25rem',
      },
      inset: {
        '1/2': '50%',
        full: '100%',
      },
      margin: {
        '-1.5': '-0.375rem',
        '-0.5': '-0.125rem',
        '0.5': '0.125rem',
        '1.5': '0.375rem',
        '2.5': '0.625rem',
        '3.5': '0.875rem',
        '4.5': '1.125rem',
        11: '2.75rem',
        18: '4.5rem',
      },
      maxHeight: {
        56: '14rem',
        64: '16rem',
        72: '18rem',
      },
      maxWidth: {
        '7xl': '80rem',
        'screen-2xl': '90rem',
      },
      rotate: {
        15: '15deg',
      },
      width: {
        '1.5': '0.375rem',
        '2.5': '0.625rem',
        '3.5': '0.875rem',
        9: '2.25rem',
        11: '2.75rem',
        17: '4.25rem',
        18: '4.5rem',
        19: '4.75rem',
        '19.5': '4.875rem',
        22: '5.5rem',
        41: '10.25rem',
        80: '20rem',
      },
      height: {
        1.5: '0.375rem',
        2.5: '0.625rem',
        3.5: '0.875rem',
        9: '2.25rem',
        11: '2.75rem',
        13: '3.25rem',
        18: '4.5rem',
        22: '5.5rem',
        31: '7.5rem',
      },
      top: {
        full: '100%',
      },
      fontSize: {
        '2xs': '.5rem',
      },
      space: {
        7: '1.75rem',
        '19.5': '4.875rem',
      },
      fill: theme => ({
        'beige-100': theme('colors.beige.100'),
        'beige-200': theme('colors.beige.200'),
        'beige-300': theme('colors.beige.300'),
        'gray-100': theme('colors.gray.100'),
        'gray-200': theme('colors.gray.200'),
      })
    },
  },
  variants: {
    backgroundColor: ['responsive', 'even', 'odd', 'hover', 'group-hover', 'focus'],
    textColor: ['responsive', 'hover', 'focus', 'group-hover'],
    display: ['responsive', 'group-hover', 'group-focus'],
    scale: ['responsive', 'hover', 'group-hover'],
    textDecoration: ['hover', 'group-hover'],
    pointerEvents: ['hover', 'group-hover'],
    transition: ['hover', 'group-hover'],
    transitionProperty: ['motion-safe', 'motion-reduce'],
    translate: ['hover', 'group-hover'],
    transform: ['hover', 'group-hover'],
    duration: ['hover', 'group-hover'],
    opacity: ['hover', 'group-hover'],
    rotate: ['hover', 'group-hover'],
  },
  plugins: [
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/custom-forms'),
    plugin(function({ addVariant }) {
      addVariant('important', ({ container }) => {
        container.walkRules(rule => {
          rule.selector = `.\\!${rule.selector.slice(1)}`
          rule.walkDecls(decl => {
            decl.important = true
          })
        })
      })
    }),
  ],
}
